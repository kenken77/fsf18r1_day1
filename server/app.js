console.log("Hello World !");
var x  = 1+1;
console.log(x);

var y;
console.log(y);

var yy = null;
console.log(yy);

var amount = 10;
console.log(amount);
console.log(typeof amount);
amount = true;
console.log(amount);
console.log(typeof amount);


var x = 5;
const x2 = 10;
function A(){
    let x = true;
    let x2 = 100;
    console.log("Inside A " + x);
    console.log(x2);
}

A();
console.log(x);
console.log(x2);

var firstName = "Kenneth";
var lastName = 'Kenneth';
console.log(typeof lastName);

var person = {
    'first name' : 'Ken',
    age : 40,
    gender : 'Male',
    fruits: ['Apple', 'Orange']
}

console.log(person['first name']);
console.log(person.fruits[1]);

var fruits = ['durian', 1, true]

console.log(fruits);

console.log(fruits[1]);

console.log(fruits[4]);

console.log(fruits.length);

console.log(fruits.indexOf(1));
var yy = 'Kenneth';
switch (yy){
    case 'Kenneth':
        console.log("its Ken");
        break;
    case 10:
        console.log("its Aileen");
        break;
    default:
        console.log('everything !');
}

var x = true;
if(x){
    console.log('its x');
}else{
    console.log('not x');
}

function hi1(){
    console.log('hi 1');
}

function hi2(){
    console.log('hi 2');
}

function hiAll(cb1, cb2){
    cb1();
    cb2();
}

hiAll(hi1, hi2);

setTimeout(function(){
    hiAll(hi1, hi2);
}, 5000);
